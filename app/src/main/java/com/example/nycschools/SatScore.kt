package com.example.nycschools

data class SatScore(
    val dbn: String,
    val school_name: String,
    val num_of_sat_test_takers: Number,
    val sat_critical_reading_avg_score: Number,
    val sat_math_avg_score: Number,
    val sat_writing_avg_score: Number,
)
