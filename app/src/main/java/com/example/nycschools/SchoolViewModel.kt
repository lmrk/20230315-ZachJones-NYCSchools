package com.example.nycschools

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.retrofitmodels.HighSchoolApi
import com.example.nycschools.retrofitmodels.RetrofitHelper
import com.example.nycschools.retrofitmodels.SatScoreApi
import kotlinx.coroutines.launch

/**
 * ViewModel for fetching and displaying school information.
 *
 * @property highSchoolApi A Retrofit instance used to fetch a list of [HighSchool]
 * @property satScoreApi A Retrofit instance used to fetch the SAT Score data for a specific school
 */
class SchoolViewModel(
    private val highSchoolApi: HighSchoolApi = RetrofitHelper.getInstance()
        .create(HighSchoolApi::class.java),
    private val satScoreApi: SatScoreApi = RetrofitHelper.getInstance()
        .create(SatScoreApi::class.java)
) : ViewModel() {

    // LiveData
    private val _highSchoolList: MutableLiveData<List<HighSchool>> =
        MutableLiveData<List<HighSchool>>()
    val highSchoolList: LiveData<List<HighSchool>> = _highSchoolList

    init {
        // Fetch list of schools upon initialization
        fetchListOfSchools()
    }
    /**
     * Fetches the list of schools from [highSchoolApi], and updates [highSchoolList] LiveData     *
     */
    fun fetchListOfSchools() {
        viewModelScope.launch {
            val schoolResult = highSchoolApi.getHighSchools()
            if (schoolResult.body() != null) {
                val schoolList = schoolResult.body()
                if (schoolList != null) {
                    _highSchoolList.postValue(schoolList)
                }
            }
        }
    }

    /**
     * Fetches SAT Score data from [satScoreApi] based on passed [schoolId] parameter, which CAN be null.
     * Returns a LiveData to which a school's SAT score (if any) will be posted.
     *
     * @param schoolId School's unique identifier for fetching SAT score info
     * @return [LiveData] to which a school's [SatScore] will be posted
     */
    fun getSatScoresForSchool(schoolId: String): LiveData<SatScore> {
        val satLiveData: MutableLiveData<SatScore> = MutableLiveData()
        viewModelScope.launch {
            val satResult = satScoreApi.getSatScoresForSchool(schoolId).body()
            if (satResult != null && satResult.isNotEmpty()) {
                satLiveData.postValue(satResult[0])
            }
        }
        return satLiveData
    }
}