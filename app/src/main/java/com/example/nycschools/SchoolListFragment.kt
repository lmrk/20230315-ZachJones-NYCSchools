package com.example.nycschools

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * A fragment representing a list of Items.
 */
class SchoolListFragment : Fragment() {
    private val schoolViewModel = SchoolViewModel()
    private var highSchoolList: List<HighSchool> = listOf()
    private lateinit var recyclerView: RecyclerView
    private lateinit var schoolListAdapter: SchoolListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        recyclerView = inflater.inflate(R.layout.school_list, container, false) as RecyclerView
        schoolListAdapter = SchoolListAdapter(highSchoolList, ::getSatScoresForSchool)

        schoolViewModel.fetchListOfSchools()
        schoolViewModel.highSchoolList.observe(viewLifecycleOwner) {
            highSchoolList = it
            schoolListAdapter.setHighSchoolList(highSchoolList)
        }

        // Set the adapter
        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = schoolListAdapter
        }
        return recyclerView
    }

    private fun getSatScoresForSchool(schoolId: String, position: Int) {
        schoolViewModel.getSatScoresForSchool(schoolId).observe(viewLifecycleOwner) { scores ->
            if (scores != null) {
                highSchoolList[position].satScores = scores
                schoolListAdapter.updateSatInfo(
                    recyclerView.findViewHolderForAdapterPosition(
                        position
                    ) as SchoolListAdapter.SchoolInfoViewHolder, scores
                )
            }
        }
    }
}