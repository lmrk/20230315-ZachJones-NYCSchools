package com.example.nycschools

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.databinding.SchoolInfoItemBinding

/**
 * [RecyclerView.Adapter] that can display a [HighSchool]'s name and [SatScore] information.
 *
 * @property highSchoolList List of [HighSchool]s to be shown
 * @property schoolClickCallback Callback when a school is clicked. Used to fetch SAT data from backend.
 */
class SchoolListAdapter(
    private var highSchoolList: List<HighSchool>,
    private val schoolClickCallback: (schoolId: String, position: Int) -> Unit
) : RecyclerView.Adapter<SchoolListAdapter.SchoolInfoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolInfoViewHolder {
        val view = SchoolInfoItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return SchoolInfoViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolInfoViewHolder, position: Int) {
        val highSchool = highSchoolList[position]

        holder.schoolNameTextView.text = highSchool.school_name
        if (highSchool.satScores != null) {
            updateSatInfo(holder, highSchool.satScores)
        }
        holder.card.setOnClickListener { expandClickListener(holder, position) }
    }

    override fun getItemCount(): Int = highSchoolList.size

    // Overridden to maintain expanded / contracted state, while not carrying state over to recycled Views
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    // Overridden to maintain expanded / contracted state, while not carrying state over to recycled Views
    override fun getItemViewType(position: Int): Int {
        return position
    }

    /**
     * Used to update list of high schools. Will call notifyDataSetChanged() to update recyclerview.
     */
    @SuppressLint("NotifyDataSetChanged")
    fun setHighSchoolList(highSchoolList: List<HighSchool>) {
        this.highSchoolList = highSchoolList
        notifyDataSetChanged()
    }

    private fun expandClickListener(holder: SchoolInfoViewHolder, position: Int) {
        holder.isExpanded = !holder.isExpanded
        // Expand / contract card by making the extra info visible / gone, respectively
        if (holder.isExpanded) {
            // Check if scores are already available. If not, fetch scores from backend.
            val scores = highSchoolList[position].satScores
            if (scores == null) {
                schoolClickCallback(highSchoolList[position].dbn, position)
            }
            holder.extraInfo.visibility = View.VISIBLE
            holder.icon.setImageResource(R.drawable.baseline_expand_less_24)
        } else {
            holder.extraInfo.visibility = View.GONE
            holder.icon.setImageResource(R.drawable.baseline_expand_more_24)
        }
    }

    /**
     * Update SAT Score information for a specific ViewHolder
     *
     * @param holder [SchoolInfoViewHolder] to be updated
     * @param scores [SatScore] info to be populated to the passed ViewHolder
     */
    fun updateSatInfo(holder: SchoolInfoViewHolder, scores: SatScore) {
        with(holder) {
            satReadingScoreTextView.text = scores.sat_critical_reading_avg_score.toString()
            satWritingScoreTextView.text = scores.sat_writing_avg_score.toString()
            satMathScoreTextView.text = scores.sat_math_avg_score.toString()
        }
    }

    inner class SchoolInfoViewHolder(binding: SchoolInfoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var isExpanded = false
        val card = binding.infoCard
        val schoolNameTextView: TextView = binding.txtSchoolName
        val extraInfo = binding.extraInfoLayout
        val icon = binding.imgExpansionIcon
        val satReadingScoreTextView = binding.txtSatReadingScore
        val satWritingScoreTextView = binding.txtSatWritingScore
        val satMathScoreTextView = binding.txtSatMathScore
    }

}