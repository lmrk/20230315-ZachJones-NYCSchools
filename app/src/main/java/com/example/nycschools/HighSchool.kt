package com.example.nycschools

data class HighSchool(
    val dbn: String,
    val school_name: String,
    var satScores: SatScore
)
