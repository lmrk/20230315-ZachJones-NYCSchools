package com.example.nycschools.retrofitmodels

import com.example.nycschools.SatScore
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SatScoreApi {
    @GET("f9bf-2cp4.json")
    suspend fun getAllSatScores(): Response<List<SatScore>>

    @GET("f9bf-2cp4.json")
    suspend fun getSatScoresForSchool(@Query("dbn") dbn: String): Response<List<SatScore>>
}