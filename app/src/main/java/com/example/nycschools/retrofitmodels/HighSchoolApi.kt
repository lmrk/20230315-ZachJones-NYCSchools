package com.example.nycschools.retrofitmodels

import com.example.nycschools.HighSchool
import retrofit2.Response
import retrofit2.http.GET

interface HighSchoolApi {
    @GET("s3k6-pzi2.json")
    suspend fun getHighSchools(): Response<List<HighSchool>>
}