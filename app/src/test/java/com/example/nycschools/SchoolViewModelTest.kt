package com.example.nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools.retrofitmodels.HighSchoolApi
import com.example.nycschools.retrofitmodels.SatScoreApi
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runner.RunWith
import org.junit.runners.model.Statement
import org.mockito.Mockito.anyString
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.stub
import retrofit2.Response

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class SchoolViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val mockHighSchoolApi = mock(HighSchoolApi::class.java)
    private val mockSatScoreApi = mock(SatScoreApi::class.java)
    private lateinit var viewModelUnderTest: SchoolViewModel

    private val testSatScore = SatScore("12345", "TEST", 10, 100, 200, 300)
    private val testHighSchool = HighSchool("12345", "TEST", testSatScore)
    private val highSchoolResponse = Response.success(listOf(testHighSchool))
    private val satResponse = Response.success(listOf(testSatScore))

    @Before
    fun setup() {
        mockHighSchoolApi.stub {
            onBlocking { getHighSchools() }.doReturn(highSchoolResponse)
        }
        mockSatScoreApi.stub {
            onBlocking { getSatScoresForSchool(anyString()) }.doReturn(
                satResponse
            )
        }
        viewModelUnderTest = SchoolViewModel(mockHighSchoolApi, mockSatScoreApi)
    }

    @Test
    fun viewModelFetchesCorrectSchoolData() = runTest {
        advanceUntilIdle()
        assertNotNull(viewModelUnderTest.highSchoolList.value)
        assertEquals(viewModelUnderTest.highSchoolList.value!![0], testHighSchool)
    }

    @Test
    fun viewModelFetchesCorrectScoreData() = runTest {
        val satScore = viewModelUnderTest.getSatScoresForSchool("12345")
        advanceUntilIdle()
        assertNotNull(satScore.value)
        assertEquals(satScore.value, testSatScore);
    }
}

@ExperimentalCoroutinesApi
class TestCoroutineRule : TestRule {

    private val testCoroutineDispatcher = StandardTestDispatcher()

    override fun apply(base: Statement, description: Description): Statement =
        object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                Dispatchers.setMain(testCoroutineDispatcher)
                base.evaluate()
                Dispatchers.resetMain()
            }
        }
}